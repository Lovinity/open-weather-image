import { createCanvas, SKRSContext2D, Image } from "@napi-rs/canvas";
import { readFile } from "fs/promises";
import {
  icon,
  uvIndexServeness,
  dir,
  font,
  timestampConverter,
  capitaliseFirstLetter,
  grabData,
  getDaytimeAndColours,
  convertToKPH,
  applyText,
  roundTo2,
  rain,
  isImperial,
} from "./utils/helperFunctions";
import { Theme } from "./utils/theme";
import { OpenWeatherArgs, TempUnit, ThemeInput } from "./utils/types";
import type { DailyWeather, Everything, HourlyWeather } from "openweather-api-node";

const currentHeight = 320;
const forecastHeight = 140;

const canvasWidth = 520;
let canvasHeight: number;

let dayTime: boolean;

let tempUnit: TempUnit;
let tempLabel: string;

let leftColour: string;
let rightColour: string;
let textColour: string;

let forecastBgColour: string;
let forecastBoxColour: string;
let forecastText: string;
let forecastBoxDivider: string;

const createWeatherImage = async (args: OpenWeatherArgs) => {
  const { bufferOutput, tempUnit, withForecast, theme, locationName } = args;

  const forecastResponse = await grabData(args);

  await setupVariables(
    forecastResponse,
    withForecast,
    theme ? theme : new Theme(),
    tempUnit
  );

  const canvas = createCanvas(canvasWidth, canvasHeight);
  const ctx = canvas.getContext("2d");

  drawBackground(ctx, withForecast);
  await drawCurrent(ctx, forecastResponse, locationName);
  await drawHourly(ctx, forecastResponse);
  withForecast && (await drawForecast(ctx, forecastResponse));

  return bufferOutput ? canvas.toBuffer("image/png") : canvas.toDataURL();
};

const drawCurrent = async (
  ctx: SKRSContext2D,
  forecastResponse: Everything,
  locationName: string
) => {
  const { current, daily, timezone } = forecastResponse;

  const currentWeather = current.weather;

  const today = daily[0];

  let leftPos: number;

  ctx.fillStyle = textColour;
  ctx.strokeStyle = textColour;

  leftPos = 22;

  const title: string = `${locationName}`;
  applyText(ctx, title, canvasWidth * (2 / 3) - leftPos, 32);
  ctx.fillText(title, leftPos, 62);

  ctx.font = font(16);
  const { date, time } = timestampConverter(current.dtRaw, timezone);
  ctx.fillText(`${date} // ${time}`, leftPos, 88);

  ctx.lineWidth = 0.5;
  ctx.beginPath();
  ctx.lineTo(15, 100);
  ctx.lineTo(304, 100);
  ctx.stroke();

  ctx.font = font(44);
  const currentTemp: string = `${Math.round(
    currentWeather.temp.cur
  )}${tempLabel}`;
  ctx.fillText(currentTemp, leftPos + 100, 145);

  ctx.font = font(16);
  ctx.fillText(
    `Feels Like: ${Math.round(currentWeather.feelsLike.cur)}${tempLabel}`,
    leftPos + 100,
    170
  );

  const imgTodayFile = dayTime
    ? await readFile(icon(today.weather.icon.raw))
    : await readFile(icon(today.weather.icon.raw.replace("d", "n")));
  const imgToday = new Image();
  imgToday.src = imgTodayFile;

  ctx.drawImage(imgToday, 353, 50, 60, 60);

  ctx.textAlign = "center";

  ctx.font = font(32);
  ctx.fillText("Today", 435, 35);

  ctx.textAlign = "left";
  ctx.font = font(14);

  ctx.fillText(capitaliseFirstLetter(today.weather.description), 420, 75);

  ctx.fillText(
    `${Math.round(today.weather.temp.min)}${tempLabel} / ${Math.round(
      today.weather.temp.max
    )}${tempLabel}`,
    420,
    100
  );

  ctx.fillText(
    capitaliseFirstLetter(currentWeather.description),
    leftPos + 100,
    191
  );

  const imgCurrentFile = await readFile(icon(currentWeather.icon.raw));
  const imgCurrent = new Image();
  imgCurrent.src = imgCurrentFile;

  ctx.drawImage(imgCurrent, leftPos, 110, 80, 80);

  ctx.beginPath();
  ctx.lineTo(15, 200);
  ctx.lineTo(304, 200);
  ctx.stroke();

  ctx.font = font(12);

  const { time: sunrise } = timestampConverter(
    current.astronomical.sunriseRaw,
    timezone
  );
  const { time: sunset } = timestampConverter(
    current.astronomical.sunsetRaw,
    timezone
  );

  const windSpeed: string = isImperial(tempUnit)
    ? `${roundTo2(currentWeather.wind.speed)} MPH`
    : `${roundTo2(convertToKPH(currentWeather.wind.speed))} KPH`;

  const windGust: string = !isNaN(currentWeather.wind.gust)
    ? isImperial(tempUnit)
      ? `(Gust: ${roundTo2(currentWeather.wind.gust)} MPH)`
      : `(Gust: ${roundTo2(convertToKPH(currentWeather.wind.gust))} KPH)`
    : "";

  ctx.fillText(
    `Wind: ${windSpeed} (${dir(currentWeather.wind.deg)}) ${windGust}`,
    leftPos,
    218
  );
  ctx.fillText(`Humidity: ${currentWeather.humidity}%`, leftPos, 233);
  ctx.fillText(
    `UV Index: ${currentWeather.uvi} (${uvIndexServeness(currentWeather.uvi)})`,
    leftPos,
    248
  );
  ctx.fillText(
    `Chance of Precip: ${Math.round(today.weather.pop * 100)}%`,
    leftPos,
    263
  );

  const nextLeftPos = canvasWidth / 3 + 26;
  let upPosRain = 278;

  if (today.weather.rain >= 0.01) {
    ctx.fillText(
      `Today's Rain: ${rain(today.weather.rain, tempUnit)}`,
      leftPos,
      upPosRain
    );
    if (currentWeather.rain < 0.01) {
      upPosRain += 15;
    }
  }

  if (currentWeather.rain >= 0.01) {
    ctx.fillText(
      `Rain Rate (hr): ${rain(currentWeather.rain, tempUnit)}`,
      nextLeftPos - 15,
      upPosRain
    );
    upPosRain += 15;
  }

  if (today.weather.snow >= 0.01) {
    ctx.fillText(
      `Today's Snow: ${rain(today.weather.snow, tempUnit)}`,
      leftPos,
      upPosRain
    );
  }

  if (currentWeather.snow >= 0.01) {
    ctx.fillText(
      `Snow Rate (hr): ${rain(currentWeather.snow, tempUnit)}`,
      nextLeftPos - 15,
      263
    );
  }

  const imgSunriseFile = await readFile(
    icon(dayTime ? "sunrised" : "sunrisen")
  );
  const imgSunrise = new Image();
  imgSunrise.src = imgSunriseFile;

  const imgSunsetFile = await readFile(icon(dayTime ? "sunsetd" : "sunsetn"));
  const imgSunset = new Image();
  imgSunset.src = imgSunsetFile;

  ctx.drawImage(imgSunrise, nextLeftPos, 224, 44, 22);
  ctx.fillText(sunrise, nextLeftPos + 52, 241);

  ctx.drawImage(imgSunset, nextLeftPos, 254, 44, 22);
  ctx.fillText(sunset, nextLeftPos + 52, 271);
};

const drawHourly = async (
  ctx: SKRSContext2D,
  forecastResponse: Everything
) => {
  const { hourly, timezone } = forecastResponse;

  ctx.fillStyle = textColour;
  ctx.strokeStyle = textColour;

  ctx.beginPath();
  ctx.lineTo(360, 120);
  ctx.lineTo(510, 120);
  ctx.stroke();

  // Hourly for the first 4 hours
  for (let i = 0; i <= 3; i++) {
    await drawHourlyBox(ctx, hourly[i], timezone, i);
  }

  // Every 3 hours after
  for (let i = 6; i <= 23; i += 3) {
    await drawHourlyBox(ctx, hourly[i], timezone, (3 + (i / 3)) - 1);
  }
};

const drawHourlyBox = async (
  ctx: SKRSContext2D,
  hourForecast: HourlyWeather,
  timezone: string,
  boxNum: number
) => {
  const { dtRaw, weather } = hourForecast;

  ctx.font = font(13);

  ctx.textAlign = "right";
  const { date, time } = timestampConverter(dtRaw, timezone);
  ctx.fillText(time, 415, 140 + (18 * boxNum), 65);

  const imgForecastFile = await readFile(icon(weather.icon.raw));
  const imgForecast = new Image();
  imgForecast.src = imgForecastFile;
  ctx.drawImage(imgForecast, 435, 128 + (18 * boxNum), 15, 15);

  ctx.textAlign = "left";
  ctx.fillText(
    `${Math.round(weather.temp.cur)}${tempLabel}`,
    470,
    140 + (18 * boxNum)
  );
};

const drawForecast = async (
  ctx: SKRSContext2D,
  forecastResponse: Everything
) => {
  const { daily, timezone } = forecastResponse;

  for (let i = 0; i < 4; i++) {
    await drawForecastBox(ctx, daily[i + 1], timezone, i);
  }
};

const drawForecastBox = async (
  ctx: SKRSContext2D,
  dayForecast: DailyWeather,
  timezone: string,
  boxNum: number
) => {
  ctx.fillStyle = forecastBoxColour;
  ctx.strokeStyle = forecastBoxDivider;

  ctx.textAlign = "center";

  ctx.lineWidth = 1;

  const topPadding: number = 15;
  const sidePadding: number = 12;

  const boxWidth = canvasWidth / 4;
  const boxBottom = canvasHeight - topPadding;

  const leftPos = boxWidth * boxNum + sidePadding;
  const centre = boxWidth * boxNum + boxWidth / 2;
  let topPos = currentHeight + topPadding;

  const { dtRaw, weather } = dayForecast;
  const { min, max } = weather.temp;
  const { description, icon: forecastIcon } = weather;
  const { date } = timestampConverter(dtRaw, timezone);

  if (boxNum !== 0) {
    ctx.beginPath();
    ctx.lineTo(boxWidth * boxNum, topPos);
    ctx.lineTo(boxWidth * boxNum, canvasHeight - topPadding);
    ctx.stroke();
  }

  ctx.fillRect(
    leftPos,
    topPos,
    boxWidth - sidePadding * 2,
    forecastHeight - topPadding * 2
  );

  ctx.fillStyle = forecastText;
  ctx.font = font(12);

  ctx.fillText(date, centre, (topPos += 12), boxWidth - sidePadding * 2);

  topPos += 12;

  const imgForecastFile = await readFile(icon(forecastIcon.raw));
  const imgForecast = new Image();
  imgForecast.src = imgForecastFile;
  ctx.drawImage(imgForecast, centre - 20, topPos, 40, 40);

  ctx.font = font(10);
  ctx.fillText(
    capitaliseFirstLetter(description),
    centre,
    (topPos += 56),
    boxWidth - sidePadding * 2
  );

  ctx.fillText(
    `${Math.round(min)}${tempLabel} / ${Math.round(max)}${tempLabel}`,
    centre,
    boxBottom - 6
  );
};

const drawBackground = async (
  ctx: SKRSContext2D,
  forecast: boolean = false
) => {
  ctx.fillStyle = leftColour;
  ctx.fillRect(0, 0, canvasWidth * (2 / 3) + 1, currentHeight);

  ctx.fillStyle = rightColour;
  ctx.fillRect(canvasWidth * (2 / 3), 0, canvasWidth, currentHeight);

  if (forecast) {
    ctx.fillStyle = forecastBgColour;
    ctx.fillRect(0, currentHeight, canvasWidth, canvasHeight);
  }
};

const setupVariables = async (
  forecastResponse: Everything,
  withForecast: boolean = false,
  theme: Theme,
  tempUnit: TempUnit
) => {
  const {
    dayTime: dt,
    leftColour: lc,
    rightColour: rc,
    textColour: tc,
    forecastBgTheme: fbg,
    forecastBoxTheme: fbox,
    forecastText: ft,
    forecastBoxDivider: fboxd,
  } = await getDaytimeAndColours({
    forecastResponse,
    theme,
  });

  dayTime = dt;
  leftColour = lc;
  rightColour = rc;
  textColour = tc;
  forecastBgColour = fbg;
  forecastBoxColour = fbox;
  forecastText = ft;
  forecastBoxDivider = fboxd;

  tempLabel = isImperial(tempUnit) ? "°F" : "°C";

  canvasHeight = withForecast ? currentHeight + forecastHeight : currentHeight;
};

export { OpenWeatherArgs, createWeatherImage, ThemeInput, Theme };
