import {
    BaseOpenWeatherArgs,
    DaytimeAndColourArgs,
    DaytimeAndColours,
    TempUnit,
    TimeLocalised,
} from './types';
import { join } from 'path';
import { SKRSContext2D } from '@napi-rs/canvas';
import { OpenWeatherAPI } from "openweather-api-node";

const FAHRENHEIT_PREFERRED_COUNTRIES = ['US', 'LR', 'MM'];

export const icon = (iconCode: string) => {
    return join(__dirname, `../svg/${iconCode}.svg`);
};

export const uvIndexServeness = (uvIndex: number) => {
    let uviServeness: string;

    if (uvIndex == 0) uviServeness = 'None';
    else if (uvIndex > 0 && uvIndex <= 2.5) uviServeness = 'Low';
    else if (uvIndex > 2.5 && uvIndex <= 5.5) uviServeness = 'Moderate';
    else if (uvIndex > 5.5 && uvIndex <= 7.5) uviServeness = 'High';
    else if (uvIndex > 7.5 && uvIndex <= 10.5) uviServeness = 'Very High';
    else if (uvIndex > 10.5) uviServeness = 'Extreme';
    else uviServeness = 'Error';

    return uviServeness;
};

export const dir = (deg: number) => {
    let dir: string;

    if (deg >= 0 && deg < 22.5) dir = 'N';
    else if (deg >= 22.5 && deg < 67.5) dir = 'NE';
    else if (deg >= 67.5 && deg < 112.5) dir = 'E';
    else if (deg >= 112.5 && deg < 157.5) dir = 'SE';
    else if (deg >= 157.5 && deg < 202.5) dir = 'S';
    else if (deg >= 202.5 && deg < 247.5) dir = 'SW';
    else if (deg >= 247.5 && deg < 292.5) dir = 'W';
    else if (deg >= 292.5 && deg < 337.5) dir = `NW`;
    else if (deg >= 337.5 && deg <= 360) dir = 'N';
    else dir = 'Error';

    return dir;
};

export const font = (size: number, name: string = 'Arial') => {
    return `${size}px ${name}`;
};

export const timestampConverter = (
    timestamp: number,
    timeZone: string
): TimeLocalised => {
    const date = new Date(timestamp * 1000);

    const timeOptions: Intl.DateTimeFormatOptions = {
        timeStyle: 'short',
        timeZone,
    };
    const dateOptions: Intl.DateTimeFormatOptions = {
        weekday: 'short',
        month: 'long',
        day: 'numeric',
        timeZone,
    };

    return {
        date: new Intl.DateTimeFormat('en-AU', dateOptions)
            .format(date)
            .replace(',', ''),
        time: new Intl.DateTimeFormat('en-AU', timeOptions).format(date),
    };
};

export const capitaliseFirstLetter = (string: string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

export const convertToKPH = (metrePerSec: number) => {
    return metrePerSec * 3.6;
};

export const grabData = async (args: BaseOpenWeatherArgs) => {
    const { key, lat, lon, tempUnit } = args;

    let weather = new OpenWeatherAPI({
        key: key,
        units: "imperial"
    });
    weather.setLocationByCoordinates(lat, lon);

    return weather.getEverything();
};

export const getDaytimeAndColours = async (
    args: DaytimeAndColourArgs
): Promise<DaytimeAndColours> => {
    const { forecastResponse, theme } = args;

    const { current } = forecastResponse;
    const { dt } = current;
    const {sunrise, sunset} = current.astronomical;

    const dayTime = dt >= sunrise && dt < sunset;
    let textColour: string;
    let leftColour: string;
    let rightColour: string;
    const {
        forecastBgTheme,
        forecastBoxTheme,
        forecastText,
        forecastBoxDivider,
    } = theme;

    if (dayTime) {
        textColour = theme.dayThemeText;
        leftColour = theme.dayThemeLeft;
        rightColour = theme.dayThemeRight;
    } else {
        textColour = theme.nightThemeText;
        leftColour = theme.nightThemeLeft;
        rightColour = theme.nightThemeRight;
    }

    return {
        dayTime,
        textColour,
        leftColour,
        rightColour,
        forecastBgTheme,
        forecastBoxTheme,
        forecastText,
        forecastBoxDivider,
    };
};

export const applyText = (
    ctx: SKRSContext2D,
    text: string,
    areaWidth: number,
    fontSize: number
) => {
    do {
        ctx.font = font(fontSize);
        fontSize -= 2;
    } while (ctx.measureText(text).width > areaWidth);

    return ctx.font;
};

export const roundTo2 = (number: number): number => {
    return parseFloat(number.toFixed(2));
};

const mmToIn = (number: number): number => {
    return number / 25.4;
};

export const rain = (rainVolume: number, tempUnit: TempUnit) => {
    return isImperial(tempUnit)
        ? `${roundTo2(mmToIn(rainVolume))} in`
        : `${rainVolume} mm`;
};

export const getTempUnitForCountry = (country: string): TempUnit => {
    return FAHRENHEIT_PREFERRED_COUNTRIES.includes(country)
        ? 'imperial'
        : 'metric';
};

export const isImperial = (tempUnit: TempUnit) => {
    return tempUnit === 'imperial';
};
